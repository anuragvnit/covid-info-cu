import React, { useEffect } from 'react';
import {
  ChakraProvider,
  Grid,
  extendTheme
} from '@chakra-ui/react';
// import { ColorModeSwitcher } from './ColorModeSwitcher';
// import { Logo } from './Logo';
import Header from "./components/header"
import Previous from "./previousApp"
import { Divider } from "@chakra-ui/react"
import ReactGA from 'react-ga'


const config = {
  initialColorMode: "dark",
  useSystemColorMode: false,
}
const theme = extendTheme({ config })
function App() {

  useEffect(() => {

    ReactGA.initialize('UA-196769358-1');
    ReactGA.pageview('/');


  }, [])
  return (
    <ChakraProvider theme={theme}>

      <Grid minH="100vh" >
        <Header />
        <Divider />

        <Previous></Previous>
      </Grid>

    </ChakraProvider>
  );
}

export default App;
