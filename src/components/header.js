import React from "react";
import { Link, Box, Flex, Text, Stack, Image } from "@chakra-ui/react";
import { ColorModeSwitcher } from '../ColorModeSwitcher'

import { FaTwitter, FaLinkedin, FaYoutube } from "react-icons/fa";
import { Badge } from "@chakra-ui/react"
import companyLogo from "../assets/cls700x700.png"
const NavBar = (props) => {
  const [isOpen, setIsOpen] = React.useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div


    >
      <NavBarContainer {...props} >
        <MenuToggle toggle={toggle} isOpen={isOpen} />
        <MenuLinks isOpen={isOpen} />
      </NavBarContainer>
    </div>
  );
};

const CloseIcon = () => (
  <svg width="24" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
    <title>Close</title>
    <path
      fill="white"
      d="M9.00023 7.58599L13.9502 2.63599L15.3642 4.04999L10.4142 8.99999L15.3642 13.95L13.9502 15.364L9.00023 10.414L4.05023 15.364L2.63623 13.95L7.58623 8.99999L2.63623 4.04999L4.05023 2.63599L9.00023 7.58599Z"
    />
  </svg>
);

const MenuIcon = () => (
  <svg
    width="24px"
    viewBox="0 0 20 20"
    xmlns="http://www.w3.org/2000/svg"
    fill="white"
  >
    <title>Menu</title>
    <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
  </svg>
);

const MenuToggle = ({ toggle, isOpen }) => {
  return (
    <Box display={{ base: "block", md: "none" }} onClick={toggle}>
      {isOpen ? <CloseIcon /> : <MenuIcon />}
    </Box>
  );
};

const MenuItem = ({ children, isLast, to = "/", ...rest }) => {
  return (

    <Text display="block" {...rest}>
      {children}
    </Text>

  );
};

const MenuLinks = ({ isOpen }) => {
  return (
    <Box
      display={{ base: isOpen ? "block" : "none", md: "block" }}
      flexBasis={{ base: "100%", md: "auto" }}
    >
      <Stack
        spacing={8}
        align="center"
        justify={["center", "space-between", "flex-end", "flex-end"]}
        direction={["column", "row", "row", "row"]}
        pt={[4, 4, 4, 4]}
      >

        <div> <Badge>To be updated</Badge>How to use the site </div>
        <Link isExternal href="https://www.linkedin.com/company/coalescence-tech/mycompany/"><MenuItem>   Get in touch </MenuItem></Link>
        <div style={{ display: 'flex', justifyContent: 'space-evenly', alignItems: "center" }}> Authored by: <Link isExternal href="https://twitter.com/_yayawar"><FaTwitter size={"2em"} color="#FFFFF"></FaTwitter></Link> <Link isExternal href="https://www.linkedin.com/in/anurag-anand-b303b464/"><FaLinkedin size={"2em"} color="#0A66C2"></FaLinkedin></Link><Link isExternal href="https://www.youtube.com/channel/UCpPOSiqKw-YaONXB4eBwzQA"><FaYoutube size={"2em"} color="#FF0000"></FaYoutube></Link> </div>
        <MenuItem>   <ColorModeSwitcher justifySelf="flex-end" /></MenuItem>


      </Stack>
    </Box >
  );
};

const NavBarContainer = ({ children, ...props }) => {
  return (
    <Flex
      as="nav"
      align="center"
      justify="space-between"
      wrap="wrap"
      w="100%"
      mb={8}
      p={8}
      bg={["primary.500", "primary.500", "transparent", "transparent"]}
      color={["white", "white", "primary.700", "primary.700"]}
      {...props}
    >
      {!props.isOpen && <MenuItem> Powered By <Link isExternal href="https://www.coalescence.tech/"> <Image
        boxSize="100px"
        objectFit="cover"
        borderRadius={60}
        src={companyLogo}
        alt="https://www.coalescence.tech/"
      ></Image> </Link> </MenuItem>}
      {children}
    </Flex>
  );
};

export default NavBar;
