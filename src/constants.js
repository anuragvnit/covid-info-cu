export const environment = "prod"

export const baseApi = environment === "dev" ? "http://localhost:9000" : "https://api.realtimecovidresources.in"