import { ColorModeScript } from '@chakra-ui/react';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import './table.css'
import reportWebVitals from './reportWebVitals';
import * as serviceWorker from './serviceWorker';
// import ReactGA from 'react-ga';
// ReactGA.initialize('UA-195775642-1');
// ReactGA.pageview(window.location.pathname + window.location.search);
ReactDOM.render(
  <div>
    <ColorModeScript />
    <App />
  </div>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorker.unregister();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
