import React, { Component } from 'react'
import { Service } from './provider'
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton, Button, Link, Box, Skeleton, Stack, Center, Badge
} from "@chakra-ui/react"
import { CircleArrow as ScrollUpButton } from "react-scroll-up-button";
import { Tabs, TabList, TabPanels, Tab, TabPanel, InputLeftElement, Tag } from "@chakra-ui/react"

import { FaSearch } from 'react-icons/fa';

import { motion } from "framer-motion"
import { Select, Heading } from "@chakra-ui/react"

import { Table, Thead, Tbody, Tr, Th, Td } from "./table";
import { TableCaption } from "@chakra-ui/react";

import { LinkBox, LinkOverlay, InputGroup, Input } from "@chakra-ui/react"
const MotionBox = motion(Box)


function Example() {
  return (
    <MotionBox
      as="aside"
      animate={{
        scale: [1, 2, 2, 1, 1],
        rotate: [0, 0, 270, 270, 0],
        borderRadius: [
          "20%",
          "20%",
          "50%",
          "50%",
          "20%"
        ]
      }}
      transition={{
        duration: 2,
        ease: "easeInOut",
        times: [0, 0.2, 0.5, 0.8, 1],
        repeat: Infinity,
        repeatType: "loop",
        repeatDelay: 1
      }}
      padding="2"
      bgGradient="linear(to-l, #1a365d, #2a69ac)"
      width="25"
      height="25"
      display="flex"
    />
  )
}
const options = [
  {
    "value": "hospital_beds",
    "label": "Hospital Beds"
  },
  {
    "value": "plasma_donors",
    "label": "Plasma Donors"
  },
  {
    "value": "oxygen_list",
    "label": "Oxygen List"
  },
  {
    "value": "medicine_list",
    "label": "Medicine List"
  },
  {
    "value": "food_list",
    "label": "Food List"
  },
  {
    "value": "home_nurse_list",
    "label": "Home Nurse List"
  },
  {
    "value": "testing_list",
    "label": "Testing List"
  },
  {
    "value": "ambulance_list",
    "label": "Ambulance List"
  },
  {
    "value": "whatsapp_telegram_group",
    "label": "Whatsapp Telegram Group"
  }
]


// class SheetRenderer extends PureComponent {
//   render() {
//     const { className, columns } = this.props
//     return (
//       <table className={className}>
//         <thead>
//           <tr>
//             <th className='cell read-only row-handle' key='$$actionCell' />
//             {
//               columns.map((col, index) => (

//                 <th style={{ width: col.width }}>{col.label}</th>
//               ))
//             }
//           </tr>
//         </thead>
//         <tbody>
//           {this.props.children}
//         </tbody>
//       </table>
//     )
//   }
// }
// const RowRenderer = (props) => {
//   const { isOver, children } = props
//   const className = isOver ? 'drop-target' : ''
//   return <tr className={className}>
//     {<td className='cell read-only row-handle' key='$$actionCell' />}
//     {children}
//   </tr>

// }
// const keysToBehandled = [{ val: "verified", label: "Available \n and \n  Verified" }, { val: "need_verification", label: "Need Verification" }, { val: "busy", label: "Busy" }, { val: "discarded", label: "Entries couldn't be parsed" }]
const keysToBehandled = [{ val: "verified", label: "Available \n and \n  Verified" }, { val: "discarded", label: "Entries couldn't be parsed" }]

// const convertOnlySelectedToDataGridFormatAndReturnAsDictOfcolumnArrAndData = (responseObj, val_name) => {
//   let isHeaderSet = false
//   let ret_val = { sheet_url: responseObj.sheet_url, updatedAt: responseObj.updatedAt, data: [], headers: [] }
//   const item = val_name
//   if (responseObj[item].length > 0) {
//     responseObj[item].forEach(element => {
//       if (isHeaderSet) {
//         ret_val.data = [...ret_val.data, { "Verification Status": item, ...element }]

//       } else {
//         ret_val.headers = ["Verification Status", ...Object.keys(element)]
//         isHeaderSet = true
//       }
//     });

//   } else {

//   }

//   console.log(ret_val, "YY")
//   return ret_val
// }

// const convertToDataGridFormatAndReturnAsDictOfcolumnArrAndData = (responseObj) => {
//   let isHeaderSet = false
//   let ret_val = { sheet_url: responseObj.sheet_url, updatedAt: responseObj.updatedAt, data: [], headers: [] }
//   keysToBehandled.forEach(item => {
//     if (responseObj[item].length > 0) {
//       responseObj[item].forEach(element => {
//         if (isHeaderSet) {
//           ret_val.data = [...ret_val.data, { "Verification Status": item, ...element }]

//         } else {
//           ret_val.headers = ["Verification Status", ...Object.keys(element)]
//           isHeaderSet = true
//         }
//       });

//     } else {

//     }
//   })
//   console.log(ret_val, "YY")
//   return ret_val
// }
// const getHeaderArrayWidthJsonFromArr = (headerArr, array_val = {}) => {
//   let oneitemRatio;
//   let sumOfCharacters = 1;
//   console.log(array_val)
//   const array_val1 = Object.values(array_val)
//   sumOfCharacters = array_val1.length === 0 ? headerArr.reduce((acc, item) => acc += item.length, 0) : array_val1.reduce((acc, item) => acc += item.length, 0)
//   console.log(sumOfCharacters, "Y")
//   // sumOfCharacters = sumOfCharacters > 0 ? sumofCharacters : 1
//   // oneitemRatio = Math.floor((100 / sumofCharacters))
//   if (array_val.length === 0) {
//     return headerArr.map(x => {
//       return {
//         label: x,
//         width: `${oneitemRatio * x.length}%`
//       }
//     })
//   } else {
//     return array_val1.map((y, index) => {
//       return {
//         label: headerArr[index],
//         width: `${Math.floor((y.length / sumOfCharacters) * 100)}%`
//       }
//     })
//   }

// }

const EachRowFromColumnArrAndRowObj = ({ rowObj, columnArr }) => {
  // const columnWithoutLable = columnArr.map(x => x.label)
  return <Tr>{Object.keys(rowObj).map((key_name, index) => (<Td key={index}>{rowObj[key_name]}</Td>))}</Tr>
}
export default class PreApp extends Component {

  constructor() {
    super();
    this.state = {
      selectedOption: null,
      disclaimer: false,
      lastUpdated: 0,
      columns: [],
      sourceSHeet: "",
      selectedLabel: "",
      selectedVal: "verified",
      prevResObj: null,
      grid: [

      ],
      cgrid: [],
      isLoaded: true, searchText: "", checked: false
    };
    this.timer = null;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.searchText !== this.state.searchText) {
      this.handleCheck();
    }
  }
  handleCheck = () => {
    // Clears running timer and starts a new one each time the user types
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.toggleCheck();
    }, 200);
  }
  toggleCheck = () => {
    this.setState(prevState => ({ checked: !prevState.checked }));
  }
  handleChange = selectedOption => {

    this.setState({ selectedOption, isLoaded: false });
    console.log(`Option selected:`, selectedOption);
    console.log("SEEELEEC", selectedOption)
    Service.getResponseFromMethodName({
      "method_name": selectedOption.value,
      "resource_type": this.state.selectedVal,
      "dev_token": "b970682b-418e-4386-84aa-ebe14122bae0"
    })
      .then(res => {
        console.log(res, "next method", selectedOption)

        let ret_val = {}
        let gridArr
        if (!!res) {
          ret_val = res
          gridArr = ret_val.data
          this.setState({ isLoaded: true, prevResObj: res, selectedLabel: selectedOption.label, sourceSHeet: ret_val.sheet_url, lastUpdated: ret_val.updatedAt, columns: gridArr.length > 0 ? Object.keys(gridArr[0]) : [], grid: gridArr, cgrid: gridArr })

        } else {
          alert("Error getting sheet info please reload/choose a different option from dropdown")
          this.setState({
            isLoaded: true
          })
        }

      })
      .catch(err => {
        // console.log(err)
        this.setState({
          isLoaded: true
        })
        alert("Error getting sheet info please reload/choose a different option  from dropdown")
      })

  };
  componentDidMount() {
    // Service.getResponseFromMethodName({
    //   "method_name": "plasma_donors",
    //   "dev_token": "b970682b-418e-4386-84aa-ebe14122bae0"
    // })
    //   .then(res => {
    //     console.log(res)
    //   })
    //   .catch(err => {
    //     console.log(err)
    //   })
  }
  setDisclamair = (valDis) => {
    this.setState({ disclaimer: valDis })
  }

  // renderSheet = (props) => {
  //   return <SheetRenderer columns={this.state.columns}  {...props} />
  // }

  // renderRow = (props) => {
  //   const { row, cells, ...rest } = props
  //   return <RowRenderer rowIndex={row} onRowDrop={this.handleRowDrop} {...rest} />
  // }
  setNewTypeValue = (val_name) => {



    if (!!this.state.selectedOption) {


      Service.getResponseFromMethodName({
        "method_name": this.state.selectedOption.value,
        "resource_type": val_name,
        "dev_token": "b970682b-418e-4386-84aa-ebe14122bae0"
      })
        .then(res => {
          // console.log(res, "next method", selectedOption)

          let ret_val = {}
          let gridArr
          if (!!res) {
            ret_val = res
            gridArr = ret_val.data
            this.setState({ isLoaded: true, prevResObj: res, selectedLabel: this.state.selectedLabel, sourceSHeet: ret_val.sheet_url, lastUpdated: ret_val.updatedAt, columns: gridArr.length > 0 ? Object.keys(gridArr[0]) : [], grid: gridArr, cgrid: gridArr })

          } else {
            alert("Error getting sheet info please reload/choose a different option from dropdown")
            this.setState({
              isLoaded: true
            })
          }

        })
        .catch(err => {
          // console.log(err)
          this.setState({
            isLoaded: true
          })
          alert("Error getting sheet info please reload/choose a different option  from dropdown")
        })

      // let ret_val = this.state.prevResObj
      // let gridArr = ret_val.data
      // this.setState({ isLoaded: true, columns: getHeaderArrayWidthJsonFromArr(gridArr.length > 0 ? Object.keys(gridArr[0]) : []), grid: gridArr, cgrid: gridArr })
    }

  }
  onChange = (e) => {
    // console.log(e.target)
    this.setState({ selectedVal: e, isLoaded: false }, () => {
      this.setNewTypeValue(this.state.selectedVal)

    })
  }
  handleSearch = (e) => {
    this.setState({
      searchText: e.target.value
    });
    let text = e.target.value.toUpperCase()
    console.log(text, "search")
    let filteredGrid = this.state.cgrid.filter((item, index) => {

      const ret_val = Object.values(item).reduce((acc, item1) => {
        // console.log(item1.toUpperCase(), text, item1.toUpperCase().indexOf(text) >= 0)
        acc = acc || (item1.toUpperCase().indexOf(text) >= 0)
        return acc
      }, false)
      return ret_val


    })
    this.setState({ grid: filteredGrid })


  }
  anyMethod = (e) => {
    console.log(e.target.value)
    alert("ye chala", JSON.stringify())
  }
  render() {
    // const { selectedOption } = this.state;
    console.log(this.state)
    return (
      <div style={{


        minHeight: "100vh"
      }}>
        <div style={{ margin: "0 5vw" }}>
          <header style={{ paddingTop: "3vh" }}>Disclaimer:This Website is just aggregating the information which is sourced in different sheets here </header>


          <Link isExternal style={{ color: "red", fontSize: "2em" }} href="https://docs.google.com/spreadsheets/d/1DtDiCcRxTzYXQIIVbgO33kjpdEF_45HFVhaKGDpz5X0/edit#gid=1046715542"><Badge >Tap to see</Badge> Sheets currently Integrated on the platform</Link>
          <Link href="https://external.sprinklr.com/insights/explorer/dashboard/601b9e214c7a6b689d76f493/tab/2?id=DASHBOARD_601b9e214c7a6b689d76f493&home=1" isExternal>
            <Heading size="2vh">Also check out this amazing <div style={{ display: "inline", color: "red", borderBottom: "1px" }}>tweet dashboard </div> which inspired me to made this site.


              </Heading>

          </Link>



          <Modal isOpen={this.state.disclaimer} onClose={() => this.setDisclamair(true)}>
            <ModalOverlay />
            <ModalContent>
              <ModalHeader>Modal Title</ModalHeader>
              <ModalCloseButton />
              <ModalBody>
                <p style={{ fontSize: 25, fontweight: "bold", maxHeight: "30vh", overflow: "scroll" }}>
                  The user understands that the information contained in these links, provides details like the contact numbers etc of hospitals, agencies etc, the contents of which are not within the control of the admininstrator of the site and Admininstrator has no responsibility for the same.  The user agrees to check and verify any/all information relied upon basis in these links before acting upon the same. The user understands that Administrator hold no liability or responsibility for real time verification of the same.













      </p>
              </ModalBody>

              <ModalFooter>
                <Button colorScheme="ghost" mr={3} onClick={() => this.setDisclamair(true)}>
                  No
            </Button>
                <Button onClick={() => this.setDisclamair(false)} >I Understand</Button>
              </ModalFooter>
            </ModalContent>
          </Modal>


        </div>

        {!this.state.disclaimer && <main>
          <div >
            <div style={{ width: "100vw", marginTop: "2vh", display: "grid", placeContent: "center" }}>
              <Center w="60vw">
                <Select placeholder=" Service to get" variant="filled" size="md" onChange={(e) => this.handleChange(options.find(x => x.value === e.target.value))}>

                  {options.map((x, y) => (
                    <option key={y}
                      value={x.value}




                    >{x.label}</option>
                  ))}


                </Select>

              </Center>


              {!this.state.isLoaded && <Example></Example>}



            </div>
            {this.state.isLoaded && (
              <div style={{ maxWidth: "90vw", padding: "4vh 5vw 1vh 5vw", margin: "auto" }}>

                <Stack spacing={12}>


                  <InputGroup>

                    <InputLeftElement
                      pointerEvents="none"
                      children={<FaSearch color="gray.300" />}
                    />
                    <Input type="text" placeholder="Search"

                      onChange={this.handleSearch}


                    />
                  </InputGroup>
                </Stack>

              </div>
            )}

          </div>
          {!!this.state.prevResObj && <div>




            <LinkBox as="article" maxW="100vw" p="5" borderWidth="1px" rounded="md">
              <Tag size={"lg"} variant="solid" colorScheme="teal">
                Last Updated : {((Date.now() - this.state.lastUpdated) / (1000 * 60)).toFixed(1)} minutes Ago
    </Tag>
              <Heading size="md" my="2">
                <LinkOverlay href={"#"}>
                  Source Sheet : {this.state.selectedLabel}
                </LinkOverlay>
              </Heading>
              <Link href={this.state.sourceSHeet} isExternal>{this.state.sourceSHeet}</Link>
            </LinkBox>

            <Tabs variant="enclosed">
              <TabList>
                {keysToBehandled.map((x, index) =>
                  <Tab key={index} onClick={() => this.onChange(x.val)}>{x.label}</Tab>
                )}
              </TabList>
              <TabPanels>
                {keysToBehandled.map((x, index) =>
                  <TabPanel>
                    {this.state.isLoaded ? <Table key={index} variant="striped" colorScheme="blue" size="md">
                      <TableCaption placeContent="top">{"N/A"}</TableCaption>
                      <Thead>
                        <Tr style={{ border: "1px solid" }}>
                          {this.state.columns.map((item, index) => (<Th key={index}>{item}</Th>))}
                        </Tr>
                      </Thead>
                      <Tbody>
                        {this.state.grid.map((item, index) => (
                          <EachRowFromColumnArrAndRowObj key={index} rowObj={item} columnArr={this.state.columns} />


                        ))}
                      </Tbody>
                    </Table> : <Stack>
                      {new Array(30).fill(0).map((y, z) => <Skeleton key={z} height="20px" />)}

                    </Stack>}
                  </TabPanel>
                )}
              </TabPanels>
            </Tabs>

          </div>}


        </main>}
        <ScrollUpButton />
      </div >
    )
  }
}

