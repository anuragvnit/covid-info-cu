import { baseApi } from './constants';
import axios from 'axios';

const createInstance = function () {
  const axiosInstance = axios.create({
    baseURL: baseApi,

  });
  return axiosInstance;
};

export const Service = {
  getResponseFromMethodName: (payload) => {
    console.log("came here")
    return new Promise(function (resolve, reject) {
      const axiosInstance = createInstance();
      axiosInstance
        .post('/api', payload)
        .then(function (response) {
          // console.log(response);
          resolve(response.data);
        })
        .catch(error => {
          // console.log(error);
          reject(error.response);
        });
    });
  }
}